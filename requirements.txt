bs4==4.5.3
re===2.2.1
requests==2.13.0
csv==1.0
tinys3==0.1.12
numpy==1.11.3
boto3==1.4.4
json==2.0.9
pandas==0.19.2