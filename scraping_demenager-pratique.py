# demande si le script est le livrable principal
# demander s'il y a contrainte en temps
# demander quels infos il veut en plus parce qu'il n'y a pas les horaires

import requests
from lxml import html


def fillData(data_dico,data_fill):
    for key in data_fill:
        data_dico[key] = data_fill[key]
    return data_dico


class Scraper:
    def __init__(self, url="https://www.demenager-pratique.com/annuaire-demenageurs.html", data_post = None):
        self.name = "Déménager-Pratique"
        self.url = url
        self.data_post = data_post or {
            "search":1,
            "task":"display",
            "searching":1,
            "searchword":"recherche...",
            "region_num":''
            }
        self.link = dict()
    
    def getRegionIndex(self):
        request = requests.get(self.url)
        tree = html.fromstring(request.content)
        name_region = tree.xpath("//select[@name='region_num']/option/text()")[1:]
        index_region = tree.xpath("//select[@name='region_num']/option/@value")[1:]
        name_region.append('Etranger')
        index_region.append('Etranger')
        name_region.append('DOM-TOM')
        index_region.append('DOM-TOM')
        return list(zip(name_region,index_region))
    
    def getLinksByRegion(self, index_region):
        if index_region == 'Etranger':
            data_fill = {"region_num":'',"foreigncountry":1,"domtom":0}
        elif index_region == 'DOM-TOM':
            data_fill = {"region_num":'',"foreign_countries":0,"domtom":1}
        else:
            data_fill = {"region_num":index_region,"foreign_countries":0,"domtom":0}
        request = requests.post(self.url,data=fillData(self.data_post,data_fill))
        tree = html.fromstring(request.content)
        list_link = tree.xpath("//span[@class='adresse']/a/@href")
        return list_link

    def getAllLinks(self):
        list_region = self.getRegionIndex()
        for element in list_region:
            name_region, index_region = element
            self.link[name_region]=self.getLinksByRegion(index_region)

if __name__=='__main__':
    url = "https://www.demenager-pratique.com/annuaire-demenageurs.html"
    data_dico = {
        "search":1,
        "task":"display",
        "searching":1,
        "searchword":"recherche...",
        "region_num":''
        }
    
    DP = Scraper()
    DP.getAllLinks()
