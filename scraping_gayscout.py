import requests
from lxml import html
import csv
import random
import tinys3
import json


def GetCity(element):
    city1 = element.xpath(".//td[contains(@class,'list4')]/a/text()")
    city2 = element.xpath(".//td[contains(@class,'list4')]/text()")
    if city1 != [] and city1 != ['\xa0']:
        return city1
    elif city2 != [] and city2 != ['\xa0']:
        return city2
    else:
        return ['']


def CleanList(l):
    for i in range(len(l)):
        if l[i]==[]:
            l[i]=['']
        else:
            l[i]=[l[i][0]]
    return l


def FiltrateTag(tag):
    new_tag = []
    for item_tag in tag:
        l = [element for element in item_tag if 'star' not in element]
        new_tag.append(l)
    max_tag = max([len(element) for element in new_tag])
    
    tag = []
    for item_tag in new_tag:
        l = max_tag*['']
        l[:len(item_tag)] = item_tag[:len(item_tag)]
        tag.append(l)
    
    return tag



if __name__=='__main__':
   
    with open('settings.json','r') as data_settings:
        settings = json.load(data_settings)

    filename = "BDD_GayScout.csv"
    S3_ACCESS_KEY = settings['S3_ACCESS_KEY']
    S3_SECRET_KEY = settings['S3_SECRET_KEY']
    bucketname = settings['bucketname']


    print("Import des paramètres proxies depuis config.csv...")

    with open('config.csv', 'r') as f:
        ip_port = f.readlines()
        ip_port = [item.replace('\n','') for item in ip_port]

    list_proxy_http = [{'http':ip_port_item} for ip_port_item in ip_port]
    list_proxy_https = [{'https':ip_port_item} for ip_port_item in ip_port]



    print('Constitution de la base de données initale')
    page=0
    name, link, tag, land, city = [],[],[],[],[]
    while True:
        url = "http://www.gayscout.com/gayguide/alllinks.php?page=%d" %page
        proxy = random.choice(list_proxy_https)
        html_data = requests.get(url,proxies = proxy)
        tree = html.fromstring(html_data.content)
        line1= "//tr[@style='background-color:#d2dae0;']"
        line2 = "//tr[@style='background-color:#e7ecef;']"
        table = tree.xpath(line1)+tree.xpath(line2)
        print("> récupération des données de la page {} avec le proxy {}".format(page,proxy))
        if len(table)==0:
            break
        else:
            name += [element.xpath(".//td[contains(@class,'list2')]/a[1]/text()") for element in table if element.xpath(".//td[contains(@class,'list2')]/a[1]/span/@style") != ['text-decoration:line-through;']]
            link += [element.xpath(".//td[contains(@class,'list2')]/a[1]/@href")[0] for element in table if element.xpath(".//td[contains(@class,'list2')]/a[1]/span/@style") != ['text-decoration:line-through;']]
            tag  += [element.xpath(".//td[contains(@class,'list1')]/img/@title") for element in table if element.xpath(".//td[contains(@class,'list2')]/a[1]/span/@style") != ['text-decoration:line-through;']]
            land += [element.xpath(".//td[contains(@class,'list3')]/img/@title") for element in table if element.xpath(".//td[contains(@class,'list2')]/a[1]/span/@style") != ['text-decoration:line-through;']]
            city += [GetCity(element) for element in table if element.xpath(".//td[contains(@class,'list2')]/a[1]/span/@style") != ['text-decoration:line-through;']]
            page += 1



    print('Enrichissement de la base de données initiale')
    address, region, postal_code, web, facebook, last_verif, tel = [],[],[],[],[],[],[]
    k = 1
    for url in link:
        proxy = random.choice(list_proxy_https)
        print("Ligne no {} sur {} depuis {} avec le proxy {}".format(k,len(link),url[26:],proxy))

        html_data = requests.get(url, proxies = proxy)
        tree = html.fromstring(html_data.content)

        address.append(tree.xpath("//span[contains(@class, 'street-address')]/text()"))
        region.append(tree.xpath("//span[contains(@class, 'region')]/text()"))
        postal_code.append(tree.xpath("//span[contains(@class, 'postal-code')]/text()"))
        site = tree.xpath("//a[contains(@class,'u-url')]/@href")
        if (len(site)>0):
            if 'facebook' in site[0]:
                web.append([])
                facebook.append(site)
            else:
                web.append(site)
                facebook.append([])
        else:
            web.append([])
            facebook.append([])
        last_verif.append(tree.xpath("//span[contains(@class, 'date')]/text()")[:1])
        tel.append(tree.xpath("//span[contains(@class, 'tel')]/text()"))

        k += 1



    print("Nettoyage des données")
    land = CleanList(land)
    tag = FiltrateTag(tag)
    address = CleanList(address)
    region = CleanList(region)
    postal_code = CleanList(postal_code)
    last_verif = CleanList(last_verif)
    tel = CleanList(tel)
    web = CleanList(web)
    facebook = CleanList(facebook)

    
    
    print("Enregistrement de la base de données dans le fichier %s" %filename)
    
    header = ['Nom','Adresse','Code_Postal', 'Ville', 'Region', 'Pays', 'Categorie','Tag1','Tag2','Tag3','Tag4','Site_Web','Facebook','Telephone','Date_MAJ']

    with open(filename, 'w', newline='',encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        k=1
        writer.writerow(header)
        for i in range(len(land)):
            ligne = name[i]+address[i]+postal_code[i]+city[i]+region[i]+land[i]+tag[i]+web[i]+facebook[i]+tel[i]+last_verif[i]
            writer.writerow(ligne)
            # print("Enregistrement de la ligne no %d" %k)
            print(k)
            k+=1

    
    
    print("Upload du fichier %s dans le bucket S3 %s..." %(filename,bucketname))
    
    conn = tinys3.Connection(S3_ACCESS_KEY,S3_SECRET_KEY,default_bucket = bucketname, tls = True)

    with open(filename,'rb') as f:
        conn.upload(filename, f)

    print("Exécution réussie sans erreur.")
