#-*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import re
import csv
import random
import tinys3
import json

def getDictCode(proxy):
    url = 'https://www.ups.com/worldshiphelp/WS12/ENU/AppHelp/Codes/State_Province_Codes.htm'
    html_data = requests.get(url,proxies=proxy)
    soup = BeautifulSoup(html_data.content,'lxml')
    line = soup.find_all('tr',{'class':'whs2'})
    name = [elmt.find('p').text for elmt in line]
    code = [elmt.find_all('p')[1].text for elmt in line]
    name.remove('State')
    name.remove('Province')
    code.remove('Code')
    code.remove('Code')
    code_name = dict(zip(code,name))
    code_name['CR'] = 'Costa Rica'
    
    return code_name


def getListURL(proxy):
    """ Récupère l'adresse des sites internet de toutes les villes du sites
    http://www.gaycities.com/places/#browse """
    
    url = 'http://www.gaycities.com/places/#browse'
    html_data = requests.get(url, proxies = proxy)
    soup = BeautifulSoup(html_data.content,'lxml')
    infos = soup.find_all('li', attrs={'class':'place-list'})
    list_url = [info.find_all('a')[1]['href'] for info in infos]
    list_city = [info.find_all('a')[1].text.replace('\n','') for info in infos]
    
    return list(zip(list_city,list_url))


def getInfosOfPage(name_city,url_city,categorie,page,code_name,proxy):
    """ Récupère tous les champs qui seront stockées dans la base de données"""

    def Clear(adresse,city,code):
        """Nettoie l'adresse pour avoir, quand c'esst possible adresse et région"""
        to_delete = ['\n','\t','    ',' , ',city,code]
        for character in to_delete:
            adresse = adresse.replace(character,'')
        adresse = adresse.split(', ,')
        if len(adresse) > 1:
            return adresse[0], adresse[1]
        else:
            return adresse[0],''

    
    url = '{url_city}{categorie}/page/{page}'.format(url_city = url_city,categorie = categorie,page = page)
    html_data = requests.get(url, proxies=proxy)
    soup = BeautifulSoup(html_data.content,'lxml')
    infos = soup.find_all('li', attrs = {'class':'listing-info'})

    if infos == []:
        # Correspond au cas où on se trouve sur une page avec aucun bar listé
        return None
    
    regex = re.compile(r'([ A-Za-zç&/.-]*),([ A-Za-z&/]*)')
    element = re.match(regex,name_city)
    city, code = element.group(1) ,element.group(2).replace(' ','')

    name, desc, adresse, likes, reviews, region = [],[],[],[],[],[]
    for info in infos:
        name.append(info.find('p', attrs = {'class':'sponsor-title'}).text)
        desc.append(info.find('p', attrs = {'class':'sponsor-subtitle'}).text)
        item_adresse,item_region = Clear(info.find('p', attrs = {'class':'sponsor-addr'}).text,city,code)
        adresse.append(item_adresse)
        region.append(item_region)
        likes.append(info.find('input', attrs = {'type':'hidden'})['value'])
        reviews.append(info.find_all('input', attrs = {'type':'hidden'})[1]['value'])
    city = len(name)*[city]
    categ = len(name)*[categorie]

    if code in code_name.keys():
        region_land = code_name[code].split(',')
        if len(region_land) > 1:
            region,land = region_land[0], region_land[1]
        else:
            region,land = '', region_land[0]
        region = len(name)*[region]
        land = len(name)*[land.replace(' US','USA').replace(' CA','Canada')]
    else:
        land = len(name)*[element.group(2)]

    data = list(zip(categ,name,desc,adresse,city,region,land,likes,reviews))
    
    # Retrait des bars fermés
    copy_data=list(data)
    for item in copy_data:
        if '(CLOSED)' in item[1]:
            data.remove(item)

    return data



if __name__=='__main__':
     
    with open('settings.json','r') as data_settings:
        settings = json.load(data_settings)

    filename = "BDD_GayCities.csv"
    S3_ACCESS_KEY = settings['S3_ACCESS_KEY']
    S3_SECRET_KEY = settings['S3_SECRET_KEY']
    bucketname = settings['bucketname']

    header = ['Categorie','Nom','Description','Adresse','Ville','Region','Pays','#Likes','#Commentaire']
    list_categorie = ['bars','restaurants','hotels','shops','arts','gyms','beaches','bathhouses','organizations','tours','entertainment','publicplaces']
    
    print('Importation des données proxy depuis config.csv... \n')
    with open('config.csv', 'r') as f:
        ip_port = f.readlines()
    ip_port = [item.replace('\n','') for item in ip_port]
  
    list_proxy_http = [{'http':ip_port_item} for ip_port_item in ip_port]
    list_proxy_https = [{'https':ip_port_item} for ip_port_item in ip_port]
     
    proxy = random.choice(list_proxy_http)
    print('Obtention des codes postaux (proxy utilisé : {})... \n'.format(proxy))
    code_name=getDictCode(proxy)
  
    proxy = random.choice(list_proxy_https)
    print('Extraction des adresses Internet (proxy utilisé : {}) ...'.format(proxy))
    data_city = getListURL(proxy)

    print('Extraction des informations et constitution de la base de données...')
    with open(filename, 'w', newline='',encoding = 'utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(header)    
        for categorie in list_categorie:
            for city in data_city:
                proxy = random.choice(list_proxy_https)
                name_city,url_city = city
                print('{} --> {} (proxy utilisé : {})'.format(categorie,name_city,proxy))
                page = 1
                while True:
                    data = getInfosOfPage(name_city,url_city,categorie, page, code_name,proxy)
                    if data == None:
                        break
                    else:
                        writer.writerows(data)
                        page += 1

    print('Base de données constituée avec succès.')

    print("Upload du fichier %s dans le bucket S3 %s..." %(filename,bucketname))
    
    conn = tinys3.Connection(S3_ACCESS_KEY,S3_SECRET_KEY,default_bucket = bucketname, tls = True)

    with open(filename,'rb') as f:
        conn.upload(filename, f)
    
    print("Exécution réussie sans erreur.")