import boto3
from difflib import SequenceMatcher
import pandas as pd
import json
import numpy as np
from threading import Thread
import time

     
def GetReference(data_dict):
    """param data: dictionnaire ayant pour valeurs des DataBase
    traitement: modifie l'attribut is_reference la DataBase servant de primary
    return: element DataBase qui servira de primary"""
    element, ln = [], []
    for item in data_dict.values():
        element.append(item)
        ln.append(len(item))
    reference = element[np.argmax(ln)]
    reference.is_reference = True
    return reference

def isNotIn(key1, key2='GS'):
    """Détermine quelle ville de la DataBase n'est pas présent dans la liste de villes de
    la seconde DataBase"""
    return [item for item in data[key1].city if item not in data[key2].city]

def similarity(string1,string2):
    """"param string1, string2: chaînes de caractères
    return: flottant évaluant la similarité entre deux chaînes de caractères"""
    def CleanString(element):
        element = element.lower()
        element = element.replace('the ','')
        element = element.replace('le ','')
        element = element.replace('la ','')
        element = element.replace('les ','')
        return element
    string1 = CleanString(string1)
    string2 = CleanString(string2)
    return SequenceMatcher(a=string1,b=string2,isjunk=None).ratio()

def cleanGeneral(data):
    """Fonction de nettoyage appliquée à toutes les bases de données"""
    def replace_character(item):
        if type(item)==str:
            item = item.replace("ã","a")
            item = item.replace("ó","o")
            item = item.replace("ú","u")
            item = item.replace("í","i")
            item = item.replace("ç","c")
            item = item.replace("ñ","n")
            item = item.replace("ü","u")
        return item
            
    data = data.applymap(replace_character)
    return data

def cleanVespaRocks(data):
    new_Ville = []

    def SplitName(name):
        if type(name)==str:
            list_name =  name.split(' (')
            old_name = list_name[0]
            if len(list_name)>1:
                new_name = list_name[1]
                new_Ville.append(new_name.replace(')',''))
            else:
                new_Ville.append('')
        else:
            new_Ville.append('')
        return old_name

    data = cleanGeneral(data)
    data.Ville = data.Ville.apply(SplitName)
    data['Ville2'] = new_Ville
    return data

def cleanAidsMap(data):
    data = cleanGeneral(data)
    return data

def cleanGayCities(data):
    
    to_replace = {
        "Pays":{"United States":"USA","Great Britain":"United Kingdom"},
        "Ville":{'Bucks County & New Hope':'New Hope',
            'Washington DC':'Washington',
        'Saugatuck / Douglas':'Saugatuck-Douglas',
        'Oakland / East Bay':'Oakland',
        'Lake Tahoe':'South Lake Tahoe'}
    }

    def replaceCountry(x):
        if type(x)==str:
            x = x.replace('United States', 'USA')   
            x = x.replace('Great Britain', 'United Kingdom')
        return x
    
    def replaceCity(x):
        if type(x)==str:
            x = x.replace('Bucks County & New Hope', 'New Hope')
            x = x.replace('Washington DC', 'Washington')
            x = x.replace('Oakland / East Bay', 'Oakland')
            x = x.replace('Saugautuck / Douglas', 'Saugatuck-Douglas')
            x = x.replace('Lake Tahoe', 'South Lake Tahoe')
        return x    
     
    data = cleanGeneral(data)
    data.replace(to_replace, inplace=True)

    return data

def cleanGayScout(data):
    def replaceCountry(x):
        if type(x)==str:
            x = x.replace('Netherlands, the','Netherlands')
            x = x.replace('Russian Federation', 'Russia')
            x = x.replace('Aruba (NL)', 'Aruba')
        return x

    def replaceCity(x):
        if type(x)==str:
            x = x.replace('Tel Aviv-Yafo','Tel Aviv')
            x = x.replace('Tampa', 'Tampa Bay')
            x = x.replace('Frankfurt am Main', 'Frankfurt')
            x = x.replace('Frankfurt (ODER)', 'Frankfurt')
            x = x.replace('New York City', 'New York')
            x = x.replace('Raleigh', 'Raleigh-Durham')

        return x   

    data = cleanGeneral(data)
    data.Pays = data.Pays.map(replaceCountry)
    data.Ville = data.Ville.map(replaceCity)

    return data


class DataBase:
    """
    Classe permettant le stockage et le traitement d'une base de données.
    attribut name: chaîne de caractère contenant le nom de l'element DatBase
    attribut is_reference: booléen déterminant si la DataBase sera le primary (base de données la plus grande)
    attribut data: DataFrame stockant les données des divers bars, places, restaurants...
    attribut header: header de la base de données
    attribut city: liste des villes repertoriées dans data
    attribut country: liste des pays repertoriées dans data
    attribut ratio_name: liste de flottant: si l'élément i est un doublon, flottant évaluant la similarité entre le nom de cet élément i et celui de l'élément doublon; 0 sinon
    attribut ratio_address: liste de flottant: si l'élément i est un doublon, flottant évaluant la similarité entre l'adresse de cet élément i et celle de l'élément doublon; 0 sinon
    attribut stat: dictionnaire avec le nombre d'éléments doublons, d'éléments nouveaux et le total
    """
    def __init__(self, key='', l=None, index=None, columns=None):
        """param key:chaîne de caractère correspondant au nom du fichier tel que stocké sur S3
        param l:liste constituant la base de données si key ne correspond à aucun fichier
        param index: index de la base de données si key ne correspond à aucun fichier
        param columns: header de la base de donnéees si key ne correspont à aucun fichier"""
        self.name = key.replace('BDD_scraping_','').replace('.csv','')
        self.is_reference = False        
        self.load_data(key,l,index,columns)
        
        if "Ville" in self.data.columns:
            self.city = self.data.Ville.dropna().unique()
        else:
            self.city = np.array([], dtype=object)
        
        if "Pays" in self.data.columns:
            self.country = self.data.Pays.dropna().unique()
        else:
            self.country = np.array([], dtype=object)

        self.status = -np.ones(len(self), dtype=object)
        self.ratio_name = np.zeros(len(self), dtype=object)
        self.ratio_address = np.zeros(len(self), dtype=object)
        
    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data.ix[idx]
    
    def __contains__(self, element):
        """Evalue si un element est présent dans la base de données, en se basant
        sur la similarité entre les noms, puis sur la similarité entre les adresses"""
        filtrate = self.filtrateCity(element.Ville).Nom
        if filtrate.empty:
            return False
        getRatio = lambda string2:similarity(element.Nom,string2)
        serie_ratio = filtrate.apply(getRatio)
        if serie_ratio.max() > 0.5:
            idx_match = serie_ratio.idxmax()
            address_element = element.Adresse
            address_reference = self[idx_match].Adresse
            ratio_address = similarity(address_element, address_reference)
            if ratio_address > 0.5:
                return serie_ratio.idxmax(),serie_ratio.max(),ratio_address
            else:
                return False
        else:
            return False
    
    def __repr__(self):
        string = "Nom \t\t\t %s \n" %self.name
        string += "Nbre d'enregistrements \t %d \n" %len(self)
        string += "Extrait des données :\n"
        string += str(self.data.head())
        return string

    def load_data(self,key,l,index,columns):
        """Charge les données dans un DataFrame depuis le fichier key.
        Sinon renvoit un DataFrame avec data=l, index=index et columns=columns"""
        try:
            self.data = pd.read_csv(key, sep=';', header=0, skip_blank_lines=True)
        except FileNotFoundError:
            self.data = pd.DataFrame(l, index=index, columns=columns)
        self.header = self.data.columns        

    def clean_data(self):
        """Nettoyage des données: remplace dans un premier les valeurs manquantes par ''
        puis appelle la fonction de nettoyage spécifique à chaque DataBase"""
        self.data.fillna('',inplace=True)
        if self.name in post_traitement.keys():
            self.data = post_traitement[self.name](self.data)
        self.city = self.data.Ville.unique()
        self.country = self.data.Pays.unique()
        
    def filtrateCity(self, name_city):
        """param name_city: nom d'une ville
        return: ensemble des elements ayant pour ville name_city"""
        if name_city == 'Napa & Sonoma':
            df1 = self.data[self.data.Ville == 'Napa']
            df2 = self.data[self.data.Ville == 'Sonoma']
            return pd.concat([df1,df2])
        return self.data[self.data.Ville == name_city]

    def compare_element_with_reference(self, num_idx, reference):
        """param num_idx: index de l'element dans self.data que l'on examine
        param reference: DataBase dans lequel on va chercher l'element
        traitement: cherche element dans la DataBase reference. Si il est trouvé, met à jour
        self.status avec l'index de l'element dans reference avec lequel il est doublon et stoke dans
        self.ratio_name et ratio_adresse les ratios de similarité. Sinon self.status est mis à -1"""

        status = reference.__contains__(self[num_idx])
        if status:
            if (self.ratio_name[num_idx] < status[1]):
                self.status[num_idx] = status[0]
                self.ratio_name[num_idx] = status[1]
                self.ratio_address[num_idx] = status[2]                
        else:
            self.status[num_idx] = -1
            self.ratio_name[num_idx] = 0
            self.ratio_address[num_idx] = 0
    
    def compare_database(self, reference, start=0, end=None):
        """traitement: lance compare_element_with_reference entre les index start et end
        Par défaut, execute la fontion sur toute la base de données"""
        if end==None:
            end = len(self)
        for i in range(start,end):
            self.compare_element_with_reference(i, reference)
        self.stat = {"Not Found":0 , "Found":0, "Total":len(self.data)}
        self.stat["Not Found"] = np.sum(self.status == -1)
        self.stat["Found"] = np.sum(self.status != -1)


class FinalDataBase(DataBase):
    """
    Classe héritant de DataBase dans lequel sera stockée entre autre la base de données fusionnée.
    attribut filename: nom du fichier dans lequel sera stockée la basée de donnée
    attribut data: base de données dans lequel sera stockée le résultat de la fusion de toutes les DataBase
    """
    def __init__(self,primary, name="Merged_Data", filename="BDD_MergedData.csv"):
        """param primary: DataBase qui sera la référence initiale
        param name: nom de l'élément
        param filename: nom du fichier dans lequel sera stockée la base de données"""
        DataBase.__init__(self, key='BDD_GayScout.csv')
        self.name = name
        self.copy_data_from_reference(primary)
        self.filename = filename
    
    def copy_data_from_reference(self,primary):
        """Copie les données du primary dans data lors de l'initialisation de la FinalDataBase"""
        self.data = primary.data.copy()
        self.clean_data()
        self.header = self.data.columns
    
    def add_fields(self,database):
        """Ajoute les colonnes de la DataBase dans FinalDataBase si elles ne sont pas déjà présentes
        (rajoute des colonnes vides)"""
        to_add = [item for item  in database.header if item not in self.header]
        for field in to_add:
            self.data[field] = pd.Series([])
        self.header=self.data.columns
        return to_add

    def merge_with(self, database,start=0,end=None):
        """Fusionne data de FinalDataBase avec data de DataBase"""
        if end==None:
            end=len(database)
        # Initialisation
        database.clean_data()
        database.compare_database(self,start,end)
        to_add = self.add_fields(database)
        database.data["Statut"]=database.status
        # Fusion des données doublons
        def update(a,b):
            element = a.replace('',np.nan).dropna()
            ref = b.replace('',np.nan).dropna()
            header_to_add = [item for item in ref.index if item not in element.index]
            for item in header_to_add:
                element.loc[item]=np.nan
            element.update(ref)
            return element
        index_merge = [i for i in range(len(database.status)) if database.status[i] != -1]
        for i in index_merge:
            a = database[i]
            b = self.data.ix[database.status[i]]
            self.data.ix[database.status[i]]=update(a,b)
        # Ajout des enregistrements manquants
        data_to_add = database.data[database.data.Statut == -1]
        self.data = pd.concat([self.data,data_to_add])
        # Finalisation
        self.data.fillna('',inplace=True)
        self.data.index = list(range(len(self)))
        del database.data["Statut"]
        del self.data["Statut"]
        # Mise à jour de l'attribut header
        self.header = self.data.columns

    def save_in_csv(self):
        """Sauvegarde les données dans un fichier CSV."""
        # header_order correspond à la liste des headers souhaités avec un ordre prédéterminé
        header_order = ['Nom','Adresse','Code_Postal','Ville','Region','Pays','Categorie','Tag1','Tag2','Tag3','Tag4','Description','Telephone','Site_Web','Facebook','Twitter','Email','Fax','Divers','Date_MAJ']
        header_bdd = [item for item in header_order if item in self.header]
        self.data = self.data[header_bdd]
        self.data.to_csv(self.filename, sep=';', na_rep='NaN',header=True,encoding='utf-8',index=False)


class ThreadComparer(Thread):
    """Classe permettant de faire de la détection de doublons de manière parallèle"""
    # Non développée car ne permet pas de meilleures performances
    def __init__(self,database,reference,n_start,n_end):
        super(ThreadComparer,self).__init__()
        self.database = database
        self.reference = reference
        self.n_start = n_start
        self.n_end = n_end
    def run(self):
        self.database.compare_database(self.reference,self.n_start,self.n_end)




if __name__ == '__main__':

    shortcut = {
    "GS":"GayScout",
    "VR":"VespaRocks",
    "AM":"AidsMap",
    "GC":"GayCities"
    }

    reverse_shortcut = {
        "GayScout":"GS",
        "VespaRocks":"VR",
        "AidsMap":"AM",
        "GayCities":"GC"
    }

    post_traitement = {
        "GayScout":cleanGayScout,
        "VespaRocks":cleanVespaRocks,
        "AidsMap":cleanAidsMap,
        "GayCities":cleanGayCities
    }
    
    ### Travail en remote depuis le serveur S3
    with open('settings.json','r') as data_settings:
        settings = json.load(data_settings)

    S3_ACCESS_KEY = settings['S3_ACCESS_KEY']
    S3_SECRET_KEY = settings['S3_SECRET_KEY']
    bucketname = settings['bucketname']    
    
    client = boto3.client('s3', aws_access_key_id=S3_ACCESS_KEY, aws_secret_access_key=S3_SECRET_KEY)
    response = client.list_objects_v2(Bucket = bucketname)
    list_files = [objet['Key'] for objet in response['Contents'] if 'BDD_scraping_' in objet['Key']]

    data = dict()

    for key in list_files:
        
        client.download_file(Bucket=bucketname, Key=key, Filename=key)
        dict_key = key.replace('BDD_scraping_','').replace('.csv','')
        data[dict_key] = DataBase(key)
    
    print("Importation de %d fichiers depuis le bucket %s" %(len(list_files), bucketname))
    
    
    # ### Travail en local
    # list_files = ['BDD_AidsMap.csv', 'BDD_GayCities.csv', 'BDD_GayScout.csv', 'BDD_VespaRocks.csv']
    # data = dict()
    # for key in list_files:
    #     dict_key = key.replace('BDD_','').replace('.csv','')
    #     data[dict_key] = DataBase(key)        
    # print("Importation de %d fichiers en local" %len(list_files))
    
    
    reference = GetReference(data)
    print('Référence: %s' %reference.name)
    ans = FinalDataBase(reference)

    # for database in data.values():
    #     if not(database.is_reference):
    #         print('Fusion des données avec %s'%database.name)
    #         ans.merge_with(database)
    # print("Enregistrement dans le fichier %s"%ans.filename)
    # ans.save_in_csv()
    # print("Upload dans le bucket %s"%bucketname)
    # client.upload_file(Filename=ans.filename,Bucket=bucketname,Key=ans.filename)
