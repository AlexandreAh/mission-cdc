import requests
from lxml import html
import csv
import random
import tinys3
import json


def Order(data):
    """
    Réarrange les donnée afin de structurer la base de données selon les catégories suivantes:
    ['Ville','Nom','Type','Pays','Adresse','Site Web','Téléphone','Twitter','Facebook','Divers']
    """
    for k in range(len(data)):
        data[k] = data[k].replace('\n','')
    
    info = 10*['']
    info[:3] = data[:3]
    data = data[3:]

    if len(data)==0:
        return info

    k = 0
    while 'pays:' not in data[k]:
        k += 1
    info[4] = ' - '.join(data[:k])
    data = data[k:]

    copy_data = data.copy()

    for item in data:
        if 'tel:' in item:
            info[6] = item.replace('tel:','').replace(' ','')
            copy_data.remove(item)
        elif 'twitter' in item:
            info[7] = item
            copy_data.remove(item)
        elif 'facebook' in item:
            info[8] = item
            copy_data.remove(item)
        elif 'http' in item:
            info[5] = item
            copy_data.remove(item)
        elif 'pays:'in item:
            info[3] = item.replace('pays:','')
            copy_data.remove(item)
        
    info[9] = ' - '.join(copy_data)
    
    return info



def GetAllCities(tree_city):
    nber_city = 1
    
    while True:
        name_city = tree_city.xpath('//*[@id="main-container"]/div[1]/div[2]/div/div[%s]/a/div/div/div[1]/text()' %str(nber_city))
        link_city = tree_city.xpath('//*[@id="main-container"]/div[1]/div[2]/div/div[%s]/a/@href' %nber_city)
    
        if len(name_city) != 0:
            nber_city += 1
            yield name_city[0], link_city[0]
        else:
            break  



def GetAllPlaces(tree_place):
    nber_place = 1
    
    while True:
        name_place = tree_place.xpath('//*[@id="main-container"]/div[1]/div[2]/div/div[%s]/a/div/div[2]/div[1]/span[1]/span/text()' %str(nber_place))
        type_place = tree_place.xpath('//*[@id="main-container"]/div[1]/div[2]/div/div[%s]/a/div/div[2]/div[1]/span[2]/text()' %str(nber_place))
        link_place = tree_place.xpath('//*[@id="main-container"]/div[1]/div[2]/div/div[%s]/a/@href' %str(nber_place))

        if len(name_place) != 0:
            nber_place += 1
            yield name_place[0], type_place[0], link_place[0]
        else:
            break



def GetAllInformations(tree_info):
    text_xpath  = '//*[@id="guidesBox"]/div[2]/span/span[{}]/span/text()'
    a_xpath     = '//*[@id="guidesBox"]/div[2]/span/span[{}]/a/@href'
    info_xpath  = '//*[@id="guidesBox"]/div[2]/span/span[{}]/span/a/text()'
    info2_xpath = '//*[@id="guidesBox"]/div[2]/span/span[{}]/span/span[1]/a/text()'
    information = []
    i = 1

    while True:
        text = tree_info.xpath(text_xpath.format(i))
        a = tree_info.xpath(a_xpath.format(i))
        info = tree_info.xpath(info_xpath.format(i))
        info2 = tree_info.xpath(info2_xpath.format(i))
        if info != []:
                info = info2+info
                information += ['pays:'+info[-1]]
        elif text != []:
                information.append(text[0])
        elif a != []:
                information.append(a[0])
        else:
                if i <= 2 :
                    pass
                else :
                    break
        i += 1

    return information



def GetDatas(list_proxy_http):
    AllCities = GetAllCities(tree_city)

    for city in AllCities :
        name_city, url_city = city
        url_place = url_base + url_city + '/search/places'
        
        proxy = proxy = random.choice(list_proxy_http)
        html_data_place = requests.get(url_place, proxies = proxy)
        tree_place = html.fromstring(html_data_place.content)

        AllPlaces = GetAllPlaces(tree_place)

        for place in AllPlaces : 
            name_place, type_place, url_place = place

            url_info = url_base + url_place
            html_data_info = requests.get(url_info)
   
            tree_info = html.fromstring(html_data_info.content)

            data = [name_city,name_place,type_place]

            details = GetAllInformations(tree_info)

            data += details
            
            if len(data)==3:
                yield None, url_place, data
            else:
                yield data, url_place, proxy



if __name__=='__main__':
   
    with open('settings.json','r') as data_settings:
        settings = json.load(data_settings)

    filename = 'BDD_VespaRocks.csv'
    S3_ACCESS_KEY = settings['S3_ACCESS_KEY']
    S3_SECRET_KEY = settings['S3_SECRET_KEY']
    bucketname = settings['bucketname']

    url_base = 'https://www.vespa.rocks'

    url_city = 'https://www.vespa.rocks/buenosaires/search/cities/'

    print("Import des paramètres proxies depuis config.csv...")

    with open('config.csv', 'r') as f:
        ip_port = f.readlines()
        ip_port = [item.replace('\n','') for item in ip_port]

    list_proxy_http = [{'http':ip_port_item} for ip_port_item in ip_port]
    list_proxy_https = [{'https':ip_port_item} for ip_port_item in ip_port]

    proxy = random.choice(list_proxy_http)


    print("Initialisation du scraper avec le proxy {}".format(proxy))
    html_data_city = requests.get(url_city, proxies = proxy)
    tree_city = html.fromstring(html_data_city.content)


    print("Ouverture du fichier {} pour enregistrement".format(filename))
    header = ['Ville','Nom','Type','Pays','Adresse','Site_Web','Telephone','Twitter','Facebook','Divers']
    with open(filename, 'w', newline='', encoding = 'utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        k = 1
        AllDatas = GetDatas(list_proxy_http)
        writer.writerow(header)
        for subdata, url, proxy in AllDatas:
            if subdata:
                writer.writerow(Order(subdata))
                print("Enregistrement no {} (depuis {} avec le proxy {})".format(k,url,proxy))
                k += 1


    print("Base de données constituée avec succès ({} enregistrements)".format(k-1))

    print("Upload du fichier %s dans le bucket S3 %s..." %(filename,bucketname))
    conn = tinys3.Connection(S3_ACCESS_KEY,S3_SECRET_KEY,default_bucket = bucketname, tls = True)

    with open(filename,'rb') as f:
        conn.upload(filename, f)

    print("Exécution réussie sans erreur.")