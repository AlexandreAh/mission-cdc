#-*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import csv
import random
import tinys3
import json

with open('settings.json','r') as data_settings:
    settings = json.load(data_settings)

filename = 'BDD_AidsMap.csv'
S3_ACCESS_KEY = settings['S3_ACCESS_KEY']
S3_SECRET_KEY = settings['S3_SECRET_KEY']
bucketname = settings['bucketname']

print("Import des paramètres proxies depuis config.csv...")

with open('config.csv', 'r') as f:
    ip_port = f.readlines()
ip_port = [item.replace('\n','') for it
em in ip_port]

list_proxy_http = [{'http':ip_port_item} for ip_port_item in ip_port]
list_proxy_https = [{'https':ip_port_item} for ip_port_item in ip_port]

proxy = list_proxy_https[random.randint(0,len(list_proxy_https)-1)]

print("Récupération des informations depuis le site...")
print("----> Utilisation du proxy {}".format(proxy))
url = 'http://www.aidsmap.com/e-atlas/Services-search-results/page/1861655?type=&name=&location=&show=-1&paging=0%7C100000000'
html_data = requests.get(url, proxies = proxy)
html_data.raise_for_status()
soup = BeautifulSoup(html_data.content, 'lxml')

table_results = soup.find('ul', {'class':'ResultsList'})
list_results = table_results.find_all('li')

data = []

for element in list_results:
    item = dict()
    item['name'] = element.find('h4').text
    item['subtitle'] = element.find('h5').text
    for infos in element.find_all('div',{'class':'OrgInfo'}):
        info = infos.text.split(': ')
        item[info[0].replace(' ','')] = ' '.join(info[1].split())
    data.append(item)

print("Constitution de la base de données...")
header = ['name', 'subtitle', 'Address', 'Town/City', 'Country', 'Telephone', 'Fax', 'Email', 'Web', 'Outreach']
header_bdd = ['Nom', 'Description', 'Adresse', 'Ville', 'Pays', 'Telephone', 'Fax', 'Email','Site_Web', 'Divers']
data2 = [header_bdd]

for element in data:
    subdata2 = []
    for attribut in header:
        if attribut in element.keys():
            subdata2.append(element[attribut])
        else:
            subdata2.append('')
    data2.append(subdata2)

print("Enregistrement des informations dans le fichier %s..." %filename)
with open(filename, 'w', newline='',encoding = 'utf-8') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerows(data2)

print("Upload du fichier %s dans le bucket S3 %s..." %(filename,bucketname))
conn = tinys3.Connection(S3_ACCESS_KEY,S3_SECRET_KEY,default_bucket = bucketname, tls = True)

with open(filename,'rb') as f:
    conn.upload(filename, f)

print("Exécution réussie sans erreur.")